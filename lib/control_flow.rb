# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.split("").reject do |char|
    char.downcase == char
  end.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if (str.length & 1) == 1
    return str[(str.length / 2)]
  end
  str[str.length / 2 - 1, 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char {|char| count += 1 if VOWELS.include? char}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = num
  (result - 1).downto(2) { |i| result *= i }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr.inject("") do |result, element|
    result << element << separator
  end.chomp(separator)
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  index = 0
  str.chars.map do |char|
    if index % 2 == 1
      char.upcase!
    else
      char.downcase!
    end
    index += 1
    char
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |word|
    word.reverse! if word.length >= 5
    word
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  1.upto(n) do |index|
    if index % 3 == 0
      if index % 5 == 0
        result << "fizzbuzz"
      else
        result << "fizz"
      end
    elsif index % 5 == 0
      result << "buzz"
    else
      result << index
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  2.upto(num / 2) do |divisor|
    return false if num % divisor == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  1.upto(num / 2) { |i| result << i if num % i == 0 }
  result << num
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)

def oddball(arr)
  first_odd = nil
  first_even = nil
  arr.each do |i|
    if i % 2 == 0
      if first_even
        return first_odd if first_odd
      else
        first_even = i
      end
    elsif first_odd
      return first_even if first_even
    else
      first_odd = i
    end
  end
end
